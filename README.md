# JP43

[![Netlify Status](https://api.netlify.com/api/v1/badges/071d557b-c63b-4dbe-8cda-a74392cf1045/deploy-status)](https://app.netlify.com/sites/jp43/deploys)
[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/alyssa-web/jp43)

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hexo and [Hexo Server][hexo-server]
1. Install dependencies: `npm install`
1. Preview your site: `hexo server`
1. Add content
1. Generate your site (optional): `hexo generate`

Read more at Hexo's [documentation][].

----

Forked from @VeraKolotyuk

[hexo]: https://hexo.io
[install]: https://hexo.io/docs/index.html#Installation
[documentation]: https://hexo.io/docs/
[hexo-server]: https://hexo.io/docs/server.html
